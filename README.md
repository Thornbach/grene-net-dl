# GrENE-net DL

## Description
GrENE-net (Genomics of rapid Evolution in Novel Environments) is an experiment which started back in 2017 and went by for multiple years. The aim is to show of rapid adaptations are predictable, especially in novel future cliamtes. In the scope of this experiment a large network consisting of 45 sites distributed over Europe, South-West Asia and North America was etablished. Every participant received a seed mixture of different ecotypes of _Arabidopsis thaliana_.  
As part of the experiment, photos of the individuals in the trays were taken at different timestamps - but these could not be analyzed further due to the large number of photos. To keep the "potential" number of photos in mind, here is a simplified estimate:  

```math
45 sites * 12 trays * 5 years * 5 timestamps/year = 13500 Images
```

This makes evaluation by human labor far too costly and also too vulnerable to individual error. Therefore, the goal of this master thesis is to develop a pipeline using a deep learning framework to automate this work and reduce the error to a systematic error.

## Visuals
TBA

## Prerequisites
TBA

## Usage
TBA

## Wiki
TBA